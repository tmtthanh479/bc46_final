import {postCommentThunk } from "./thunk";
import { createSlice } from "@reduxjs/toolkit";

type quanlyAirbnbComment = {
  isCommenting: boolean;
};
const initialState: quanlyAirbnbComment = {
  isCommenting: false,
};
const postCommentSlice = createSlice({
  name: "quanLyAirbnbComment",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(postCommentThunk.pending, (state) => {
        state.isCommenting = true;
      })
      .addCase(postCommentThunk.fulfilled, (state) => {
        state.isCommenting = false;
      })
      .addCase(postCommentThunk.rejected, (state) => {
        state.isCommenting = false;
      });
  },
});
export const { reducer: postCommentReducer, actions: postCommentActions } = postCommentSlice;
